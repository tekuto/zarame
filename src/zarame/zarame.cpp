﻿#include "fg/util/export.h"
#include "zarame/zarame.h"
#include "zarame/window/mainwindowdrawevent.h"
#include "zarame/controller/raw/event.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/closeevent.h"
#include "fg/window/eventprocessor.h"
#include "fg/gl/thread.h"
#include "fg/gl/threadexecutor.h"
#include "sucrose/controller/raw/manager.h"
#include "sucrose/udev/joystick/manager.h"
#include "sucrose/udev/joystick/eventparameters.h"
#include "sucrose/udev/joystick/connectevent.h"
#include "sucrose/udev/joystick/disconnectevent.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/axisevent.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/manager.h"
#include "sucrose/udev/eventparameters.h"
#include "sucrose/udev/event.h"

#include <memory>
#include <mutex>
#include <utility>
#include <cstddef>
#include <algorithm>

namespace {
    const auto  WINDOW_TITLE = "ZARAME";
    const auto  WINDOW_WIDTH = 800;
    const auto  WINDOW_HEIGHT = 600;

    const auto  CONTROLLER_MODULE_UDEV_JOYSTICK = "sucrose-udev-joystick";

    auto createWindow(
    )
    {
        return fg::Window::create(
            WINDOW_TITLE
            , WINDOW_WIDTH
            , WINDOW_HEIGHT
        );
    }

    void glThread(
        fg::GLThread< zarame::Zarame > &    _thread
    )
    {
        auto &  zarame = _thread.getState().getData();

        auto    eventData = FgMainWindowDrawEventData{
            *( zarame.windowUnique ),
        };

        auto &  joiner = *( zarame.mainWindowDrawEventJoinerUnique );

        zarame.mainWindowDrawEventManagerUnique->execute(
            joiner
            , eventData
        );

        joiner.join();

        //TODO 後処理（システムメニューの描画など）
    }

    void paintEvent(
        fg::WindowPaintEventBackground< zarame::Zarame > &  _event
    )
    {
        _event.getState().getData().glThreadExecutorUnique->execute();
    }

    void closeEvent(
        fg::WindowCloseEventBackground< zarame::Zarame > &  _event
    )
    {
        auto &  zarame =  _event.getState().getData();

        auto    lock = std::unique_lock< std::mutex >( zarame.mutex );

        zarame.ended = true;

        zarame.cond.notify_all();
    }

    void rawControllerEventProc(
        const fg::RawControllerActionBuffer &   _ACTION_BUFFER
        , zarame::Zarame &                      _zarame
    )
    {
        auto    eventData = FgRawControllerEventData{
            *_ACTION_BUFFER,
        };

        auto &  joiner = *( _zarame.rawControllerEventJoinerUnique );

        _zarame.rawControllerEventManagerUnique->execute(
            joiner
            , eventData
        );

        joiner.join();
    }

    auto createRawControllerManager(
        fg::State< zarame::Zarame > &   _state
        , zarame::Zarame &              _zarame
    )
    {
        return sucrose::RawControllerManager::create(
            _state
            , rawControllerEventProc
            , _zarame
        );
    }

    void connectEventProc(
        const sucrose::UdevJoystickConnectEvent &   _EVENT
        , zarame::Zarame &                          _zarame
    )
    {
        const auto &    JOYSTICK_ID = _EVENT.getID();

        _zarame.rawControllerManagerUnique->connect(
            CONTROLLER_MODULE_UDEV_JOYSTICK
            , JOYSTICK_ID.getPort()
            , JOYSTICK_ID.getDevice()
        );
    }

    void disconnectEventProc(
        const sucrose::UdevJoystickDisconnectEvent &    _EVENT
        , zarame::Zarame &                              _zarame
    )
    {
        const auto &    JOYSTICK_ID = _EVENT.getID();

        _zarame.rawControllerManagerUnique->disconnect(
            CONTROLLER_MODULE_UDEV_JOYSTICK
            , JOYSTICK_ID.getPort()
            , JOYSTICK_ID.getDevice()
        );
    }

    void buttonEventProc(
        const sucrose::UdevJoystickButtonEvent &    _EVENT
        , zarame::Zarame &                          _zarame
    )
    {
        const auto &    JOYSTICK_ID = _EVENT.getID();

        const auto  PORT = JOYSTICK_ID.getPort();
        const auto  DEVICE = JOYSTICK_ID.getDevice();

        const auto  INDEX = _EVENT.getIndex();

        auto &  rawControllerManager = *( _zarame.rawControllerManagerUnique );

        if( _EVENT.getPressed() == true ) {
            rawControllerManager.pressButton(
                CONTROLLER_MODULE_UDEV_JOYSTICK
                , PORT
                , DEVICE
                , INDEX
            );
        } else {
            rawControllerManager.releaseButton(
                CONTROLLER_MODULE_UDEV_JOYSTICK
                , PORT
                , DEVICE
                , INDEX
            );
        }
    }

    void axisEventProc(
        const sucrose::UdevJoystickAxisEvent &  _EVENT
        , zarame::Zarame &                      _zarame
    )
    {
        const auto &    JOYSTICK_ID = _EVENT.getID();

        _zarame.rawControllerManagerUnique->operateAxis(
            CONTROLLER_MODULE_UDEV_JOYSTICK
            , JOYSTICK_ID.getPort()
            , JOYSTICK_ID.getDevice()
            , _EVENT.getIndex()
            , _EVENT.getValue()
        );
    }

    auto createUdevJoystickManager(
        fg::State< zarame::Zarame > &   _state
        , zarame::Zarame &              _zarame
    )
    {
        auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();

        eventParametersUnique->setEventParameters(
            _zarame
            , connectEventProc
            , disconnectEventProc
            , buttonEventProc
            , axisEventProc
        );

        return sucrose::UdevJoystickManager::create(
            _state
            , *eventParametersUnique
        );
    }

    void udevEventProc(
        const sucrose::UdevEvent &  _EVENT
        , zarame::Zarame &          _zarame
    )
    {
        _zarame.udevJoystickManagerUnique->analyzeUdevEvent( _EVENT );
    }

    auto createUdevManager(
        fg::State< zarame::Zarame >&    _state
        , zarame::Zarame &              _zarame
    )
    {
        auto    eventParametersUnique = sucrose::UdevEventParameters::create();

        eventParametersUnique->setEventParameters(
            _zarame
            , udevEventProc
        );

        return sucrose::UdevManager::create(
            _state
            , *eventParametersUnique
        );
    }

    void waitEnd(
        zarame::Zarame &    _zarame
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _zarame.mutex );

        while( _zarame.ended == false ) {
            _zarame.cond.wait( lock );
        }
    }
}

namespace zarame {
    Zarame::Zarame(
    )
        : ended( false )
    {
    }

    void Zarame::initialize(
        fg::State< Zarame > &   _state
    )
    {
        this->mainWindowDrawEventManagerUnique = fg::StateEventManager< FgMainWindowDrawEventData >::create();
        this->mainWindowDrawEventJoinerUnique = fg::StateJoiner::create();

        this->rawControllerEventManagerUnique = fg::StateEventManager< FgRawControllerEventData >::create();
        this->rawControllerEventJoinerUnique = fg::StateJoiner::create();

        auto &  windowUnique = this->windowUnique;
        windowUnique = createWindow();
        auto &  window = *windowUnique;

        auto &  windowEventManagersUnique = this->windowEventManagersUnique;
        windowEventManagersUnique = fg::WindowEventManagers::create();
        auto &  windowEventManagers = *windowEventManagersUnique;

        this->windowPaintEventRegisterManagerUnique = fg::WindowPaintEventRegisterManager::create(
            windowEventManagers.getPaintEventManager()
            , _state
            , paintEvent
        );
        this->windowCloseEventRegisterManagerUnique = fg::WindowCloseEventRegisterManager::create(
            windowEventManagers.getCloseEventManager()
            , _state
            , closeEvent
        );

        auto &  glContextUnique = this->glContextUnique;
        glContextUnique = fg::GLContext::create( window );
        auto &  glContext = *glContextUnique;

        this->glThreadExecutorUnique = fg::GLThreadExecutor::create(
            _state
            , glContext
            , glThread
        );

        this->windowEventProcessorUnique = fg::WindowEventProcessor::create(
            _state
            , window
            , windowEventManagers
        );

        this->rawControllerManagerUnique = createRawControllerManager(
            _state
            , *this
        );
        this->udevJoystickManagerUnique = createUdevJoystickManager(
            _state
            , *this
        );
        this->udevManagerUnique = createUdevManager(
            _state
            , *this
        );
    }

    void Zarame::initialize(
        fg::BasesystemState< Zarame > & _basesystemState
    )
    {
        auto &  state = _basesystemState.getState();

        auto    zarameUnique = Zarame::create();
        auto &  zarame = *zarameUnique;

        _basesystemState.setData(
            zarameUnique.release()
            , Zarame::destroy
        );

        zarame.initialize( state );

        _basesystemState.setWaitEndProc(
            waitEnd
            , zarame
        );
    }
}
