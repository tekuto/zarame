﻿#include "fg/util/export.h"
#include "zarame/controller/raw/event.h"
#include "zarame/zarame.h"
#include "fg/core/state/eventregistermanager.h"

const FgRawControllerActionBuffer * fgBasesystemRawControllerEventDataGetActionBuffer(
    const FgRawControllerEventData *    _IMPL_PTR
)
{
    return &( _IMPL_PTR->ACTION_BUFFER );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateForCreatingState(
    FgCreatingState *   _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForCreatingState(
            &**( zarame.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateForState(
    FgState *           _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForState(
            &**( zarame.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForCreatingState(
    FgCreatingState *               _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateBackgroundForCreatingState(
            &**( zarame.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgRawControllerEventRegisterManager * fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForState(
    FgState *                       _statePtr
    , FgStateEventBackgroundProc    _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgRawControllerEventRegisterManager * >(
        fgStateEventRegisterManagerCreateBackgroundForState(
            &**( zarame.rawControllerEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

void fgBasesystemRawControllerEventRegisterManagerDestroy(
    FgRawControllerEventRegisterManager *   _implPtr
)
{
    fgStateEventRegisterManagerDestroy( reinterpret_cast< FgStateEventRegisterManager * >( _implPtr ) );
}
