﻿#include "fg/util/test.h"
#include "zarame/zarame.h"
#include "fg/basesystem/window/mainwindow.h"
#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/basesystem/controller/raw/event.h"
#include "sucrose/window/window.h"
#include "brownsugar/basesystemstatemanager.h"
#include "brownsugar/core/state/basesystem.h"

#include <memory>
#include <new>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <utility>
#include <cstring>
#include <X11/Xlib.h>

TEST(
    ZarameTest
    , Initialize
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    const auto &    ZARAME = basesystemState.getState().getData();
    ASSERT_NE( nullptr, &ZARAME );

    ASSERT_FALSE( ZARAME.ended );
    ASSERT_NE( nullptr, ZARAME.mainWindowDrawEventManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.mainWindowDrawEventJoinerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.rawControllerEventManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.rawControllerEventJoinerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.windowUnique.get() );
    ASSERT_NE( nullptr, ZARAME.windowEventManagersUnique.get() );
    ASSERT_NE( nullptr, ZARAME.windowPaintEventRegisterManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.windowCloseEventRegisterManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.glContextUnique.get() );
    ASSERT_NE( nullptr, ZARAME.glThreadExecutorUnique.get() );
    ASSERT_NE( nullptr, ZARAME.windowEventProcessorUnique.get() );
    ASSERT_NE( nullptr, ZARAME.rawControllerManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.udevJoystickManagerUnique.get() );
    ASSERT_NE( nullptr, ZARAME.udevManagerUnique.get() );

    ASSERT_TRUE( basesystemState->isExistsWaitEndProc() );
}

struct MainWindowDrawEventTestData : public fg::UniqueWrapper< MainWindowDrawEventTestData >
{
    int &   i;

    fg::MainWindowDrawEventRegisterManager::Unique  registerManagerUnique;

    MainWindowDrawEventTestData(
        int &                                               _i
        , fg::MainWindowDrawEventRegisterManager::Unique && _registerManagerUnique
    )
        : i( _i )
        , registerManagerUnique( std::move( _registerManagerUnique ) )
    {
    }

    static Unique create(
        int &                                               _i
        , fg::MainWindowDrawEventRegisterManager::Unique && _registerManagerUnique
    )
    {
        return new MainWindowDrawEventTestData(
            _i
            , std::move( _registerManagerUnique )
        );
    }
};

void mainWindowDrawEventTestProc(
    fg::MainWindowDrawEvent< MainWindowDrawEventTestData > &    _event
)
{
    auto &  data = _event.getState().getData();

    data.i = 20;
}

TEST(
    ZarameTest
    , MainWindowDrawEvent
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    basesystemStateManagerUnique->start();

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< MainWindowDrawEventTestData > &  _state
        )
        {
            auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
                _state
                , mainWindowDrawEventTestProc
            );

            return MainWindowDrawEventTestData::create(
                i
                , std::move( registerManagerUnique )
            ).release();
        }
        , MainWindowDrawEventTestData::destroy
    );

    fg::getMainWindow( state ).repaint();

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    ASSERT_EQ( 20, i );
}

TEST(
    ZarameTest
    , MainWindowCloseEvent
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    auto &  state = basesystemState.getState();

    auto &  window = fg::getMainWindow( state );

    auto &  xDisplay = **( window->xDisplayForEventUnique );
    auto &  xWindow = window->xWindow;
    auto    xEvent = XEvent();
    xEvent.xclient.display = &xDisplay;
    xEvent.xclient.window = xWindow;
    xEvent.xclient.type = ClientMessage;
    xEvent.xclient.format = 32;
    std::memcpy(
        xEvent.xclient.data.l
        , &( window->wmDeleteWindow )
        , sizeof( window->wmDeleteWindow )
    );

    XSendEvent(
        &xDisplay
        , xWindow
        , False
        , 0
        , &xEvent
    );

    XFlush( &xDisplay );

    auto &  zarame = state.getData();

    const auto &    ENDED = zarame.ended;

    {
        auto    lock = std::unique_lock< std::mutex >( zarame.mutex );

        if( ENDED == false ) {
            zarame.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_TRUE( ENDED );
}

struct RawControllerEventTestData : public fg::UniqueWrapper< RawControllerEventTestData >
{
    int &   i;

    fg::RawControllerEventRegisterManager::Unique   registerManagerUnique;

    RawControllerEventTestData(
        int &                                               _i
        , fg::RawControllerEventRegisterManager::Unique && _registerManagerUnique
    )
        : i( _i )
        , registerManagerUnique( std::move( _registerManagerUnique ) )
    {
    }

    static Unique create(
        int &                                               _i
        , fg::RawControllerEventRegisterManager::Unique && _registerManagerUnique
    )
    {
        return new RawControllerEventTestData(
            _i
            , std::move( _registerManagerUnique )
        );
    }
};

void rawControllerEventTestProc(
    fg::RawControllerEvent< RawControllerEventTestData > &  _event
)
{
    auto &  data = _event.getState().getData();

    data.i = 20;
}

TEST(
    ZarameTest
    , RawControllerEvent
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    basesystemStateManagerUnique->start();

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    i = 10;

    state.enter(
        [
            &i
        ]
        (
            fg::CreatingState< RawControllerEventTestData > &   _state
        )
        {
            auto    registerManagerUnique = fg::RawControllerEventRegisterManager::create(
                _state
                , rawControllerEventTestProc
            );

            return RawControllerEventTestData::create(
                i
                , std::move( registerManagerUnique )
            ).release();
        }
        , RawControllerEventTestData::destroy
    );

    auto &  zarame = basesystemState.getState().getData();

    zarame.rawControllerManagerUnique->connect(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    ASSERT_EQ( 20, i );
}
