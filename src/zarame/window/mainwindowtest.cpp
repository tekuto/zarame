﻿#include "fg/util/test.h"
#include "fg/basesystem/window/mainwindow.h"
#include "zarame/zarame.h"
#include "fg/core/state/basesystem.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/state.h"
#include "brownsugar/basesystemstatemanager.h"

#include <X11/Xlib.h>

TEST(
    MainWindowTest
    , GetMainWindowForCreatingState
)
{
    XInitThreads();

    auto    brownsugarUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, brownsugarUnique.get() );

    auto &  basesystemState = brownsugarUnique->getBasesystemState< zarame::Zarame >();
    auto &  state = basesystemState.getState();

    zarame::Zarame::initialize( basesystemState );

    const auto &    ZARAME = state.getData();
    ASSERT_NE( nullptr, &ZARAME );

    ASSERT_EQ( ZARAME.windowUnique.get(), &( fg::getMainWindow( reinterpret_cast< fg::CreatingState<> & >( state ) ) ) );
}

TEST(
    MainWindowTest
    , GetMainWindowForState
)
{
    XInitThreads();

    auto    brownsugarUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, brownsugarUnique.get() );

    auto &  basesystemState = brownsugarUnique->getBasesystemState< zarame::Zarame >();
    auto &  state = basesystemState.getState();

    zarame::Zarame::initialize( basesystemState );

    const auto &    ZARAME = state.getData();
    ASSERT_NE( nullptr, &ZARAME );

    ASSERT_EQ( ZARAME.windowUnique.get(), &( fg::getMainWindow( state ) ) );
}
