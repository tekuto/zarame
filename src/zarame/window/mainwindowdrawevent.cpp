﻿#include "fg/util/export.h"
#include "zarame/window/mainwindowdrawevent.h"
#include "zarame/zarame.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/eventregistermanager.h"

FgWindow * fgBasesystemMainWindowDrawEventDataGetWindow(
    FgMainWindowDrawEventData * _implPtr
)
{
    return &*( _implPtr->window );
}

FgMainWindowDrawEventRegisterManager * fgBasesystemMainWindowDrawEventRegisterManagerCreateForCreatingState(
    FgCreatingState *   _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgMainWindowDrawEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForCreatingState(
            &**( zarame.mainWindowDrawEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

FgMainWindowDrawEventRegisterManager * fgBasesystemMainWindowDrawEventRegisterManagerCreateForState(
    FgState *           _statePtr
    , FgStateEventProc  _eventProcPtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );
    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return reinterpret_cast< FgMainWindowDrawEventRegisterManager * >(
        fgStateEventRegisterManagerCreateForState(
            &**( zarame.mainWindowDrawEventManagerUnique )
            , _statePtr
            , _eventProcPtr
        )
    );
}

void fgBasesystemMainWindowDrawEventRegisterManagerDestroy(
    FgMainWindowDrawEventRegisterManager *  _implPtr
)
{
    fgStateEventRegisterManagerDestroy( reinterpret_cast< FgStateEventRegisterManager * >( _implPtr ) );
}
