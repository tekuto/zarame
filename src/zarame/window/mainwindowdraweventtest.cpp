﻿#include "fg/util/test.h"
#include "zarame/window/mainwindowdrawevent.h"
#include "zarame/zarame.h"
#include "fg/window/window.h"
#include "fg/core/state/creating.h"
#include "brownsugar/basesystemstatemanager.h"

#include <X11/Xlib.h>

TEST(
    MainWindowDrawEventDataTest
    , GetWindow
)
{
    auto    windowUnique = fg::Window::create(
        "MainWindowDrawEventDataTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    dataImpl = FgMainWindowDrawEventData{ window };
    auto &  data = reinterpret_cast< fg::MainWindowDrawEventData & >( dataImpl );

    ASSERT_EQ( &window, &( data.getWindow() ) );
}

void createTestProc(
    fg::MainWindowDrawEvent<> &
)
{
}

TEST(
    MainWindowDrawEventRegisterManagerTest
    , CreateForCreatingState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
        reinterpret_cast< fg::CreatingState<> & >( state )
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}

TEST(
    MainWindowDrawEventRegisterManagerTest
    , CreateForState
)
{
    XInitThreads();

    auto    basesystemStateManagerUnique = brownsugar::BasesystemStateManager::create();
    ASSERT_NE( nullptr, basesystemStateManagerUnique.get() );

    auto &  basesystemState = basesystemStateManagerUnique->getBasesystemState< zarame::Zarame >();

    zarame::Zarame::initialize( basesystemState );

    auto &  state = basesystemStateManagerUnique->getTopState();

    auto    registerManagerUnique = fg::MainWindowDrawEventRegisterManager::create(
        state
        , createTestProc
    );
    ASSERT_NE( nullptr, registerManagerUnique.get() );
}
