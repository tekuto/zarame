﻿#include "fg/util/export.h"
#include "fg/basesystem/window/mainwindow.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/state.h"
#include "zarame/zarame.h"

FgWindow * fgBasesystemGetMainWindowForCreatingState(
    FgCreatingState *   _statePtr
)
{
    auto &  state = reinterpret_cast< fg::CreatingState<> & >( *_statePtr );

    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return &**( zarame.windowUnique );
}

FgWindow * fgBasesystemGetMainWindowForState(
    FgState *   _statePtr
)
{
    auto &  state = reinterpret_cast< fg::State<> & >( *_statePtr );

    auto &  zarame = state.getBasesystem_< zarame::Zarame >();

    return &**( zarame.windowUnique );
}
