# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

from waflib.Tools import waf_unit_test

import os.path

APPNAME = 'zarame'
VERSION = '0.8.0'

out = 'build'

taf.PACKAGE_NAME = 'zarame'

taf.LOAD_TOOLS = [
    'compiler_cxx',
    'waf_unit_test',
    'taf.tools.cpp',
]

cpp.INCLUDES = [
    os.path.join(
        '..',
        'fg',
        'inc',
    ),
    os.path.join(
        '..',
        'sucrose',
        'inc',
    ),
]

cpp.TEST_INCLUDES = [
    os.path.join(
        '..',
        'brownsugar',
        'inc',
    ),
]

cpp.TEST_LIBPATH = [
    os.path.join(
        '..',
        'brownsugar',
        'build',
        'brownsugar',
    ),
    os.path.join(
        '..',
        'sucrose',
        'build',
        'sucrose',
    ),
    os.path.join(
        '..',
        'sucrose',
        'build',
        'test',
    ),
]

taf.POST_FUNCTIONS = [
    waf_unit_test.summary,
]
