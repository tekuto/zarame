# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'zarame_files',
    'zarame_zarametest',
    'zarame_window_mainwindowtest',
    'zarame_window_mainwindowdraweventtest',
    'zarame_controller_raw_eventtest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'zarame'

module.SOURCE = [
    'zarame.cpp',
    {
        'window' : [
            'mainwindow.cpp',
            'mainwindowdrawevent.cpp',
        ],
        'controller' : {
            'raw' : [
                'event.cpp',
            ],
        },
    },
]
