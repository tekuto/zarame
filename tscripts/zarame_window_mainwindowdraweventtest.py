# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'zarame',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'zarame-window-mainwindowdraweventtest'

module.SOURCE = {
    'window' : [
        'mainwindowdraweventtest.cpp'
    ],
}

module.LIB = [
    'X11',
    'brownsugar-statemanager',
    'brownsugar-basesystemstatemanager',
    'sucrose-window',
    'sucrose-gl',
    'sucrose-udev',
    'sucrose-udev-joystick',
    'sucrose-controller',
]

module.USE = [
    'zarame',
]
