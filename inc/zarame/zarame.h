﻿#ifndef ZARAME_ZARAME_H
#define ZARAME_ZARAME_H

#include "fg/core/state/basesystem.h"
#include "fg/core/state/state.h"
#include "fg/core/state/eventmanager.h"
#include "fg/core/state/joiner.h"
#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/basesystem/controller/raw/event.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/closeevent.h"
#include "fg/window/eventprocessor.h"
#include "fg/gl/context.h"
#include "fg/gl/threadexecutor.h"
#include "fg/common/unique.h"
#include "sucrose/controller/raw/manager.h"
#include "sucrose/udev/joystick/manager.h"
#include "sucrose/udev/manager.h"
#include "fg/util/import.h"

#include <mutex>
#include <condition_variable>

namespace zarame {
    struct Zarame : public fg::UniqueWrapper< Zarame >
    {
        std::mutex              mutex;
        std::condition_variable cond;
        bool                    ended;

        fg::StateEventManager< FgMainWindowDrawEventData >::Unique  mainWindowDrawEventManagerUnique;
        fg::StateJoiner::Unique                                     mainWindowDrawEventJoinerUnique;

        fg::StateEventManager< FgRawControllerEventData >::Unique   rawControllerEventManagerUnique;
        fg::StateJoiner::Unique                                     rawControllerEventJoinerUnique;

        fg::Window::Unique  windowUnique;

        fg::WindowEventManagers::Unique             windowEventManagersUnique;
        fg::WindowPaintEventRegisterManager::Unique windowPaintEventRegisterManagerUnique;
        fg::WindowCloseEventRegisterManager::Unique windowCloseEventRegisterManagerUnique;

        fg::GLContext::Unique           glContextUnique;
        fg::GLThreadExecutor::Unique    glThreadExecutorUnique;

        fg::WindowEventProcessor::Unique    windowEventProcessorUnique;

        sucrose::RawControllerManager::Unique   rawControllerManagerUnique;

        sucrose::UdevJoystickManager::Unique    udevJoystickManagerUnique;
        sucrose::UdevManager::Unique            udevManagerUnique;

        Zarame(
        );

        inline static Unique create(
        )
        {
            return new Zarame;
        }

        void initialize(
            fg::State< Zarame > &
        );

        static FG_FUNCTION_VOID(
            initialize(
                fg::BasesystemState< Zarame > &
            )
        )
    };
}

#endif  // ZARAME_ZARAME_H
