﻿#ifndef ZARAME_WINDOW_MAINWINDOWDRAWEVENT_H
#define ZARAME_WINDOW_MAINWINDOWDRAWEVENT_H

#include "fg/basesystem/window/mainwindowdrawevent.h"
#include "fg/window/window.h"

struct FgMainWindowDrawEventData
{
    fg::Window &    window;
};

#endif  // ZARAME_WINDOW_MAINWINDOWDRAWEVENT_H
