﻿#ifndef ZARAME_CONTROLLER_RAW_EVENT_H
#define ZARAME_CONTROLLER_RAW_EVENT_H

#include "fg/basesystem/controller/raw/event.h"
#include "fg/controller/raw/actionbuffer.h"

struct FgRawControllerEventData
{
    const FgRawControllerActionBuffer & ACTION_BUFFER;
};

#endif  // ZARAME_CONTROLLER_RAW_EVENT_H
